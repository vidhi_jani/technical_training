// JavaScript Document

function check_blnk(c1, msg) {
	l = c1.value.length;

	if (l == 0) {
		alert(msg);
		return false;
	}

	return true;
}

function check_pass(c1, c2) {

	if (c1.value != c2.value) {
		alert("Confirm Password and password  must match!");
		c1.value = "";
		c2.value = "";
		c1.focus();
		return false;
	}
	return true;
}
function check_len(c1) {

	len = c1.value.length;

	if (len < 6 && len > 12) {

		alert("password must be between 6 to 12 characters");

		return false;
	}
	return true;
}
function check_num(c1) {

	if (isNaN(c1.value)) {
		alert("Plz enter valid number!");
		return false;
	}
	return true;

}
function check_gen(c1, msg) {

	if (!c1[0].checked && !c1[1].checked) {
		alert(msg);
		return false;
	}
	return true;
}

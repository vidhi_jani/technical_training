function validateForm() {

	var emailVal = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
	var nameVal = /^[A-Z a-z]*$/;
	var mobileVal = /^[1-9]{1}[0-9]{9}$/;
	// var names
	// numberRegex

	var fname = document.forms.reg.fname.value;
	if (fname == "" || fname == null) {

		document.reg.fname.focus();
		document.getElementById("errorBox").innerHTML = "First Name must be filled out";

		return false;
	} else if (!nameVal.test(fname)) {

		document.reg.fname.focus();
		document.getElementById("errorBox").innerHTML = "Enter only Alphabests in First Name";

		return false;
	}

	var lname = document.forms.reg.lname.value;
	if (lname == "" || lname == null) {
		document.reg.lname.focus();
		document.getElementById("errorBox").innerHTML = "Last Name must be filled out";

		return false;
	} else if (!nameVal.test(lname)) {

		document.reg.lname.focus();
		document.getElementById("errorBox").innerHTML = "Enter only Alphabests in Last Name";

		return false;
	}

	var b = document.forms.reg.mobile.value;
	console.log(b);
	if (b == "" || b == null) {
		document.reg.mobile.focus();
		document.getElementById("errorBox").innerHTML = "Mobile No must be filled out";

		return false;
	} else if (!mobileVal.test(b)) {
		document.reg.mobile.focus();
		document.getElementById("errorBox").innerHTML = "please Enter only Numbers of 10 digits";

		return false;
	}

	var email = document.forms.reg.email.value;
	if (email == "" || email == null) {
		document.reg.email.focus();
		document.getElementById("errorBox").innerHTML = "Email  must be filled out";
		return false;
	} else if (!emailVal.test(email)) {
		document.reg.email.focus();
		document.getElementById("errorBox").innerHTML = "Enter valid email";

		return false;
	}

	var dob = document.forms.reg.dob.value;
	if (dob == "" || dob == null) {
		document.reg.dob.focus();
		document.getElementById("errorBox").innerHTML = "Date of Birth must be filled out";

		return false;
	}

	var pass = document.forms.reg.pass.value;
	if (pass == "" || pass == null) {
		document.reg.pass.focus();
		document.getElementById("errorBox").innerHTML = "passsword must be filled out";
		return false;
	} else if (pass.length < 8) {
		document.reg.pass.focus();
		document.getElementById("errorBox").innerHTML = "Password must be at least 8 char long";
		return false;
	}

	var cpass = document.forms.reg.cpass.value;
	if (cpass == "" || cpass == null) {
		document.reg.cpass.focus();
		document.getElementById("errorBox").innerHTML = "confirm passsword must be filled out";
		return false;
	} else if (pass != cpass) {
		document.reg.cpass.focus();
		document.getElementById("errorBox").innerHTML = "Password does not  math";
		return false;
	}

	if (!document.getElementById('check').checked) {
		alert("You must agree to the terms first.");
		return false;

	} else {
		alert("Successfully submited");
	}

	// if(!document.getElementById('check').checked) {
	//
	// document.reg.check.focus();
	// document.getElementById("errorBox").innerHTML = "Oops! You forgot to
	// agree to the terms and Conditions";
	// return false;
	// }

}

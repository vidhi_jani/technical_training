        var value = '';
        var flag = false;
        var cnt = 0;
        var ft = 0;

        function calc() {
        	value = parseCalculationString(value);
        	result = calculate(value);
        	Calc.Input.value = result;
        	value = Calc.Input.value;
        	flag = true;
        }

        function parseCalculationString(s) {
        	// --- Parse a calculation string into an array of numbers and operators
        	var calculation = [],
        		current = '';
        	for (var i = 0, ch; ch = s.charAt(i); i++) {
        		if ('*/+-'.indexOf(ch) > -1) {
        			if (current == '' && ch == '-') {
        				current = '-';
        			} else {
        				calculation.push(parseFloat(current), ch);
        				current = '';
        			}
        		} else {
        			current += s.charAt(i);
        		}
        	}
        	if (current != '') {
        		calculation.push(parseFloat(current));
        	}
        	return calculation;
        }

        function calculate(calc) {
        	// --- Perform a calculation expressed as an array of operators and numbers
        	var ops = [{
        				'*': (a, b) => a * b,
        				'/': (a, b) => a / b
        			},
        			{
        				'+': (a, b) => a + b,
        				'-': (a, b) => a - b
        			}
        		],
        		newCalc = [],
        		currentOp;
        	for (var i = 0; i < ops.length; i++) {
        		for (var j = 0; j < calc.length; j++) {
        			if (ops[i][calc[j]]) {
        				currentOp = ops[i][calc[j]];
        			} else if (currentOp) {
        				newCalc[newCalc.length - 1] =
        					currentOp(newCalc[newCalc.length - 1], calc[j]);
        				currentOp = null;
        			} else {
        				newCalc.push(calc[j]);
        			}
        			console.log(newCalc);
        		}
        		calc = newCalc;
        		newCalc = [];
        	}
        	if (calc.length > 1) {
        		console.log('Error: unable to resolve calculation');
        		return calc;
        	} else {
        		return calc[0];
        	}
        }


        function add(operands) {
        	if (flag == true) {
        		cnt = 0;
        		flag = false;
        	} else if (flag == false && cnt == 0) {
        		if (ft != 1) {
        			cnt = (value.split('.').length - 1);
        		} else {
        			flag = true;
        			ft = 0;
        		}
        	} else if (flag == false && cnt > 1) {
        		cnt = 1;
        	}
        	if (operands == '.' && cnt == 1 && flag == false) {
        		value += operands;
        		if (value.slice(-1) == '.') {
        			value = value.substring(0, value.length - 1);
        			Calc.Input.value = Calc.Input.value.substring(0, Calc.Input.value.length - 1);
        		}
        	} else {
        		value += operands;

        	}

        }



        function addoperator(operator) {
        	value += operator;
        	Calc.Input.value = ' ';
        	flag = true;
        	ft = 1;
        }

        function clearscreen() {
        	value = ' ';
        	Calc.Input.value = ' ';
        	cnt = 0;
        	flag = false;
        }
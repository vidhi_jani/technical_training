var assert = chai.assert;
var expect = chai.expect;
var should = chai.should();




describe('calc', function () {  
  
  
  it('should have a function `calc`', function () {
    expect(calc()).to.be.a('function');
  });
  it('should return ans ', function () {
	  
    expect(calc(['1+2'])).to.equal(['3']);
  });

});




describe('parseCalculationString', function () {  
  it('should have a function `calc`', function () {
    expect(parseCalculationString(s)).to.be.a('function');
  });
  it('should return arrays of length < 2', function () {
	  
    expect(parseCalculationString(s)).to.equal(['3']);
  });

});

describe('clearscreen', function () {  
  it('should have a function `calc`', function () {
    expect(clearscreen()).to.be.a('function');
  });
  it('should return arrays of length < 2', function () {
	  
    expect(clearscreen()).to.equal(['3']);
  });

});
